Sensirion-sht3x-IDF
====================

This is a driver adds support for sht3x sensor by sensirion

The driver is for the usage with the ESP32 and IDF.

For now supports only Single shot mode.

Measurement process
====================

initialize the sensor with the function ```sensirion_i2c_master_init```

Once the SHT3x sensor is initialized, it can be used for measurements.

Single shot mode
====================

In single shot mode, a measurement command triggers the acquisition of exactly one data pair. Each data pair consists of temperature and humidity as 16-bit decimal values.

Due to the measurement duration of up to 15 ms, to avoid blocking use a ```xTaskCreate``` during measurements:

To start single shot mode use the function ```sht3x_single_shot```

In the single shot mode, the user task has to perform the above step every time new sensor values ​​are needed.
