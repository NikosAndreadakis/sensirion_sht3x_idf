#
# Component Makefile
#

set (SENSIRION_DIVER ${CMAKE_CURRENT_LIST_DIR})

set (COMPONENT_ADD_INCLUDEDIRS
        "${SENSIRION_DIVER}/src"
        "${SENSIRION_DIVER}/include")

set (COMPONENT_SRCDIRS
        "${SENSIRION_DIVER}/src"
)
        
set (COMPONENT_SUBMODULES "${SENSIRION_DIVER}")

register_component()

target_compile_definitions(${COMPONENT_TARGET} PUBLIC
)
