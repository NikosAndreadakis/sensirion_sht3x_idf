/*
 * sensirion_i2c_sht3x.h
 *
 * Sensirion humidity temperature sensor SHT3x driver V0.1
 * Copyright (C) 2021 Nikos Andreadakis.  All Rights Reserved.
 * Author Nikos Andreadakis
 * mail: nikosandreadakis@hotmail.com
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
 * the Software, and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 * This driver provide support for the sht3x sensor to work with ESP-IDF
 */

#include <stdint.h>


typedef int sht3x_err_t;

#define BH1750_SENSOR_ADDR CONFIG_BH1750_ADDR   /*!< slave address for BH1750 sensor */
#define BH1750_CMD_START CONFIG_BH1750_OPMODE   /*!< Operation mode */
#define ESP_SLAVE_ADDR CONFIG_I2C_SLAVE_ADDRESS /*!< ESP32 slave address, you can set any 7bit value */
#define WRITE_BIT I2C_MASTER_WRITE              /*!< I2C master write */
#define READ_BIT I2C_MASTER_READ                /*!< I2C master read */
#define ACK_CHECK_EN 0x1                        /*!< I2C master will check ack from slave*/
#define ACK_CHECK_DIS 0x0                       /*!< I2C master will not check ack from slave */
#define ACK_VAL 0x0                             /*!< I2C ack value */
#define NACK_VAL 0x1                            /*!< I2C nack value */

#define SHT_OK 0
#define SHT_ERR_BUSY 0x101
#define SHT_ERR_TX 0x102

typedef enum {
	HIGHT_ACR = 0x00,
	MEDIUM_ACR = 0x0b,
	LOW_ACR = 0x16
}sht3x_accuracy;

typedef enum {
	DISABLE = 0,
	ENABLE = 1
}sht3x_heater_switch;

void test();
sht3x_err_t sht3x_single_shot(i2c_port_t i2c_num, sht3x_accuracy accuracy, float *temp, float *hum);

/**
 * @brief The SHT3x-DIS provides a soft reset mechanism that
 * 		  forces the system into a well-defined state without
 * 		  removing the power supply.
 *
 * @param i2c_num I2C port number
 * @return
 *     - SHT_OK   Success
 *     - SHT_ERR_BUSY Sensor is busy
 *     - SHT_ERR_TX Transmite to sensor error accure
 */
sht3x_err_t sht3x_soft_rst(i2c_port_t i2c_num);

/**
 * @brief The SHT3x is equipped with an internal heater, which
 * 	  	  is meant for plausibility checking only. The temperature
 * 	  	  increase achieved by the heater depends on various
 * 	  	  parameters and lies in the range of a few degrees
 * 	  	  centigrade.
 *
 * @param i2c_num I2C port number
 * @param sht3x_heater_switch bool enable disable heater
 * @return
 *     - SHT_OK   Success
 *     - SHT_ERR_BUSY Sensor is busy
 *     - SHT_ERR_TX Transmite to sensor error accure
 */
sht3x_err_t sht3x_heater(i2c_port_t i2c_num, sht3x_heater_switch en_dis);
