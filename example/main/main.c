#include <stdio.h>
#include "esp_log.h"
#include "driver/i2c.h"
#include "sdkconfig.h"
#include "sensirion_i2c_sht3x.h"

#define _I2C_NUMBER(num) I2C_NUM_##num
#define I2C_NUMBER(num) _I2C_NUMBER(num)

#define I2C_MASTER_SCL_IO CONFIG_I2C_MASTER_SCL               /*!< gpio number for I2C master clock */
#define I2C_MASTER_SDA_IO CONFIG_I2C_MASTER_SDA               /*!< gpio number for I2C master data  */
#define I2C_MASTER_NUM I2C_NUMBER(CONFIG_I2C_MASTER_PORT_NUM) 		  /*!< I2C port number for master dev */
#define I2C_MASTER_FREQ_HZ CONFIG_I2C_MASTER_FREQUENCY        /*!< I2C master clock frequency */
#define I2C_MASTER_TX_BUF_DISABLE 0                           /*!< I2C master doesn't need buffer */
#define I2C_MASTER_RX_BUF_DISABLE 0                           /*!< I2C master doesn't need buffer */

static const char *TAG = "sht3x-example";

float sht_temp =0;
float sht_hum = 0;

/**
 * @brief i2c master initialization
 */
static esp_err_t sensirion_i2c_master_init(void)
{
    int i2c_master_port = I2C_MASTER_NUM;
    i2c_config_t conf;
    conf.mode = I2C_MODE_MASTER;
    conf.sda_io_num = I2C_MASTER_SDA_IO;
    conf.sda_pullup_en = GPIO_PULLUP_ENABLE;
    conf.scl_io_num = I2C_MASTER_SCL_IO;
    conf.scl_pullup_en = GPIO_PULLUP_ENABLE;
    conf.master.clk_speed = I2C_MASTER_FREQ_HZ;
    printf("conf.master.clk_speed: %d\n",conf.master.clk_speed);
    i2c_param_config(i2c_master_port, &conf);
    return i2c_driver_install(i2c_master_port, conf.mode, I2C_MASTER_RX_BUF_DISABLE, I2C_MASTER_TX_BUF_DISABLE, 0);
}

void app_main(void)
{
	esp_err_t ret;
	ret = sensirion_i2c_master_init();
	if(ret != ESP_OK){
		printf("i2c error\n");
	}
	vTaskDelay(1000 / portTICK_PERIOD_MS);
    while (true) {
//        test();
        sht3x_single_shot(I2C_MASTER_NUM, MEDIUM_ACR, &sht_temp, &sht_hum);
        printf("Temp: %.1f\t Humidity : %d\n",sht_temp,(int)sht_hum);

        vTaskDelay(10000 / portTICK_PERIOD_MS);
    }
}

