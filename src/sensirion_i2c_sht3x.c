/*
 * sensirion_i2c_sht3x.c
 *
 * Sensirion humidity temperature sensor SHT3x driver V0.1
 * Copyright (C) 2021 Nikos Andreadakis.  All Rights Reserved.
 * Author Nikos Andreadakis
 * mail: nikosandreadakis@hotmail.com
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
 * the Software, and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 * This driver provide support for the sht3x sensor to work with ESP-IDF
 */


#include <stdio.h>
#include "esp_log.h"
#include "driver/i2c.h"
#include "sensirion_i2c_sht3x.h"
#include "math.h"

uint8_t sensor_data_h, sensor_data_l, command;
static const char *TAG = "SHT3x";

esp_err_t send_command(i2c_port_t i2c_num,uint8_t msb, uint8_t lsb){
    int ret;
    i2c_cmd_handle_t cmd = i2c_cmd_link_create();

    i2c_master_start(cmd);
    i2c_master_write_byte(cmd, CONFIG_SHT3X_ADDRESS << 1 | WRITE_BIT, ACK_CHECK_EN); //adress send ack
    i2c_master_write_byte(cmd, msb, ACK_CHECK_EN); // send msb ack
    i2c_master_write_byte(cmd, lsb, ACK_CHECK_EN); //send lsb ack
    i2c_master_stop(cmd);
    ret = i2c_master_cmd_begin(i2c_num, cmd, 1000 / portTICK_RATE_MS);
    i2c_cmd_link_delete(cmd);
//    vTaskDelay(1 / portTICK_RATE_MS);
    return ret;
}

/*Readout of Measurement Results for Periodic Mode*/
sht3x_err_t sht3x_single_shot(i2c_port_t i2c_num, sht3x_accuracy accuracy, float *temp, float *hum){
	//receve Temp first after Humidity
	uint8_t msb_temp=0;
	uint8_t lsb_temp=0;
	uint8_t crc_temp=0;

	uint8_t msb_hum=0;
	uint8_t lsb_hum=0;
	uint8_t crc_hum=0;

	int ret;
	uint8_t i=0;

	//Measurement Commands for Single Shot Data Acquisition Mode
	uint8_t msb = 0x24;
	uint8_t lsb = 0x00;
	if(send_command(i2c_num, msb, lsb)){
		printf("Error On command send\n");
		return SHT_ERR_TX;
	}else{
		vTaskDelay(1 / portTICK_RATE_MS);

		while(true){

			if(i>2){
				return SHT_ERR_BUSY;
			}
		    //Start Read
		    i2c_cmd_handle_t cmd = i2c_cmd_link_create();
		    i2c_master_start(cmd);
		    //send address
		    i2c_master_write_byte(cmd, CONFIG_SHT3X_ADDRESS << 1 | READ_BIT, ACK_CHECK_EN);
		    //Temp firts + crc
		    i2c_master_read_byte(cmd, &msb_temp, ACK_VAL);
		    i2c_master_read_byte(cmd, &lsb_temp, ACK_VAL);
		    i2c_master_read_byte(cmd, &crc_temp, ACK_VAL);
		    //Second Humidity + crc
		    i2c_master_read_byte(cmd, &msb_hum, ACK_VAL);
		    i2c_master_read_byte(cmd, &lsb_hum, ACK_VAL);
		    i2c_master_read_byte(cmd, &crc_hum, NACK_VAL);
		    i2c_master_stop(cmd);
		    ret = i2c_master_cmd_begin(i2c_num, cmd, (10 / portTICK_RATE_MS));
		    i2c_cmd_link_delete(cmd);

		    if(ret!=ESP_OK){
		    	i++;
		    }else{
		    	uint16_t raw_temp = ((uint8_t)msb_temp << 8) + (uint8_t)lsb_temp;
		    	uint16_t raw_hum = ((uint8_t)msb_hum << 8) + (uint8_t)lsb_hum;
		    	*temp = (float)(-45.0+(175.0*raw_temp/65535));
		    	*hum = (int)(100*raw_hum/65535);
#ifdef CONFIG_SHT3X_DEBUG
		    	printf("Raw temp: %d\tRaw Humidity %d\n",raw_temp,raw_hum);
#endif
		    	return SHT_OK;
		    }
		}
	}
}

esp_err_t sht3x_soft_rst(i2c_port_t i2c_num){
	//Soft Reset / Re-Initialization
	uint8_t msb = 0x30;
	uint8_t lsb = 0xa2;
	if(send_command(i2c_num, msb, lsb)){
		return SHT_ERR_TX;
	}else {
		vTaskDelay(5 / portTICK_RATE_MS);
		return 0;
	}
}

sht3x_err_t sht3x_heater(i2c_port_t i2c_num, sht3x_heater_switch en_dis){
	uint8_t msb = 0x30; //Command
	if(en_dis){
		uint8_t lsb = 0x6d; // For enable
		return send_command(i2c_num,msb,lsb);
	}else{
		uint8_t lsb = 0x66; // For disable
		return send_command(i2c_num,msb,lsb);
	}
}



void test(){
	printf("Start driver!!\n");

}
